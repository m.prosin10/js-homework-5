// Завдання 1
function divideNumbers(a, b) {
  if (b === 0) {
    console.log("Ділення на нуль неможливе");
    return null;
  }
  return a / b;
}

const result = divideNumbers(10, 2);
console.log(result);

// Завдання 2
function getNumberInput(message) {
  let number;
  while (true) {
    const input = prompt(message);
    number = parseFloat(input);
    if (!isNaN(number)) {
      break;
    }
    alert("Введіть дійсне число!");
  }
  return number;
}

function getOperationInput() {
  const validOperations = ["+", "-", "*", "/"];
  let operation;
  while (true) {
    operation = prompt("Введіть математичну операцію (+, -, *, /):");
    if (validOperations.includes(operation)) {
      break;
    }
    alert("Такої операції не існує");
  }
  return operation;
}

function calculate(a, b, operation) {
  switch (operation) {
    case "+":
      return a + b;
    case "-":
      return a - b;
    case "*":
      return a * b;
    case "/":
      if (b === 0) {
        alert("Ділення на нуль неможливе");
        return null;
      }
      return a / b;
    default:
      alert("Невідома операція");
      return null;
  }
}

const num1 = getNumberInput("Введіть перше число:");
const num2 = getNumberInput("Введіть друге число:");
const operation = getOperationInput();

const resultation = calculate(num1, num2, operation);
console.log(`Результат: ${resultation}`);

// Завдання 3
function getNumberInput(message) {
  let number;
  while (true) {
    const input = prompt(message);
    number = parseInt(input, 10);
    if (!isNaN(number) && number >= 0) {
      break;
    }
    alert("Введіть дійсне додатнє число або нуль!");
  }
  return number;
}

function factorial(n) {
  if (n === 0) {
    return 1;
  }
  return n * factorial(n - 1);
}

const num = getNumberInput("Введіть число для підрахунку факторіалу:");
const output = factorial(num);
alert(`Факторіал ${num} дорівнює ${output}`);
